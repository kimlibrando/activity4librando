package com.example.toshiba.activity4;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {
    Button btn;
    EditText name;
    EditText pw;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        name = (EditText) findViewById(R.id.txtName);
        pw = (EditText) findViewById(R.id.txtPw);
        final Context context = this;
        btn = (Button) findViewById(R.id.btnLogin);

        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Toast.makeText(getApplicationContext(), "Logging in..",
                            Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(context, main.class);
                Bundle bundle = new Bundle();
                bundle.putString("name", name.getText().toString());
                bundle.putString("password", pw.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
                }


        });
    }
}
