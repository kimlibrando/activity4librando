package com.example.toshiba.activity4;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class main extends AppCompatActivity {
    EditText name;
    EditText pw;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        name = (EditText) findViewById(R.id.txtName);
        pw = (EditText) findViewById(R.id.txtPassword);
        Bundle bundle =this.getIntent().getExtras();
        String n=bundle.getString("name");
        String p=bundle.getString("password");
        name.setText(n);
        pw.setText(p);
    }
}
